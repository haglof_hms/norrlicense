// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program\\HMS\\LicProtector270.dll" no_namespace
// CLicProtectorEasyGo270 wrapper class

class CLicProtectorEasyGo270 : public COleDispatchDriver
{
public:
	CLicProtectorEasyGo270(){} // Calls COleDispatchDriver default constructor
	CLicProtectorEasyGo270(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CLicProtectorEasyGo270(const CLicProtectorEasyGo270& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// ILicProtectorEasyGo270 methods
public:
	long Validate(LPCTSTR sModule, LPCTSTR sUser, BOOL bUseWindowsUser)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BOOL ;
		InvokeHelper(0x1, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModule, sUser, bUseWindowsUser);
		return result;
	}
	long Prepare(LPCTSTR FileName, LPCTSTR PSecKey)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0x2, DISPATCH_METHOD, VT_I4, (void*)&result, parms, FileName, PSecKey);
		return result;
	}
	long Quit()
	{
		long result;
		InvokeHelper(0x3, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long ShowLicViewer()
	{
		long result;
		InvokeHelper(0x4, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	CString GetInstCode(long CopyProtectType)
	{
		CString result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x5, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms, CopyProtectType);
		return result;
	}
	CString ModuleName(LPCTSTR sModule)
	{
		CString result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x6, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms, sModule);
		return result;
	}
	long get_LastReturnCode()
	{
		long result;
		InvokeHelper(0x65, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long TotalLicences(LPCTSTR sModule)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x7, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModule);
		return result;
	}
	long RemainingLicences(LPCTSTR sModule)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x8, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModule);
		return result;
	}
	long RemainingDays(LPCTSTR sModule)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x9, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModule);
		return result;
	}
	long NoOfDays(LPCTSTR sModule)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0xa, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModule);
		return result;
	}
	DATE ExpiredOn(LPCTSTR sModule)
	{
		DATE result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0xb, DISPATCH_METHOD, VT_DATE, (void*)&result, parms, sModule);
		return result;
	}
	BOOL get_SysdateChanged()
	{
		BOOL result;
		InvokeHelper(0x66, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	BOOL get_LicFileChanged()
	{
		BOOL result;
		InvokeHelper(0x67, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	CString GetErrorMessage(long lError)
	{
		CString result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xc, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms, lError);
		return result;
	}
	long _CheckLicenceID(long lLicId)
	{
		long result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xd, DISPATCH_METHOD, VT_I4, (void*)&result, parms, lLicId);
		return result;
	}
	long ReadActivationKey(LPCTSTR sKey)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0xe, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sKey);
		return result;
	}
	long GetCopyProtection()
	{
		long result;
		InvokeHelper(0xc9, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long SetLanguage(long iLanguage)
	{
		long result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xca, DISPATCH_METHOD, VT_I4, (void*)&result, parms, iLanguage);
		return result;
	}
	CString GetTagValue(LPCTSTR sModule)
	{
		CString result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0xcb, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms, sModule);
		return result;
	}
	long CheckInterval(LPCTSTR sModule)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0xce, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModule);
		return result;
	}
	long DiffPct(LPCTSTR sModule)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0xcf, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModule);
		return result;
	}
	long SetDiffPct(LPCTSTR sModule, long pct)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_I4 ;
		InvokeHelper(0xcc, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModule, pct);
		return result;
	}
	long SetCheckInterval(LPCTSTR sModule, long interval)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_I4 ;
		InvokeHelper(0xcd, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModule, interval);
		return result;
	}
	long GetCUIsRunning(LPCTSTR sModule, LPCTSTR sUser, BOOL bUseWindowsUser)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BOOL ;
		InvokeHelper(0xd0, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModule, sUser, bUseWindowsUser);
		return result;
	}
	long LicfileVersion()
	{
		long result;
		InvokeHelper(0xd1, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long LicfileKey()
	{
		long result;
		InvokeHelper(0xd2, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	DATE GetCULastValidate(LPCTSTR sModule, LPCTSTR sUser, BOOL bUseWindowsUser)
	{
		DATE result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BOOL ;
		InvokeHelper(0xd3, DISPATCH_METHOD, VT_DATE, (void*)&result, parms, sModule, sUser, bUseWindowsUser);
		return result;
	}
	DATE GetCUNextValidate(LPCTSTR sModule, LPCTSTR sUser, BOOL bUseWindowsUser)
	{
		DATE result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BOOL ;
		InvokeHelper(0xd4, DISPATCH_METHOD, VT_DATE, (void*)&result, parms, sModule, sUser, bUseWindowsUser);
		return result;
	}
	long GetCURunCounter(LPCTSTR sModule, LPCTSTR sUser, BOOL bUseWindowsUser)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BOOL ;
		InvokeHelper(0xd5, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModule, sUser, bUseWindowsUser);
		return result;
	}
	BOOL get_CheckSysdateChanged()
	{
		BOOL result;
		InvokeHelper(0xd6, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_CheckSysdateChanged(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0xd6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_CheckLicChanged()
	{
		BOOL result;
		InvokeHelper(0xd7, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_CheckLicChanged(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0xd7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long StopConcurrentUsage(LPCTSTR sModule, LPCTSTR sUser, BOOL bUseWindowsUser)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BOOL ;
		InvokeHelper(0xd8, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModule, sUser, bUseWindowsUser);
		return result;
	}
	CString GetManufacturer()
	{
		CString result;
		InvokeHelper(0xd9, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	CString GetCustomer()
	{
		CString result;
		InvokeHelper(0xda, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	long ActivateEntry(LPCTSTR sModule, LPCTSTR sEntry)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0xdf, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModule, sEntry);
		return result;
	}
	long DeactivateEntry(LPCTSTR sModule, LPCTSTR sEntry)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0xe0, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModule, sEntry);
		return result;
	}
	BOOL ExistsEntry(LPCTSTR sModule, LPCTSTR sEntry)
	{
		BOOL result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0xe1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms, sModule, sEntry);
		return result;
	}
	BOOL IsEntryActivated(LPCTSTR sModule, LPCTSTR sEntry)
	{
		BOOL result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0xe2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms, sModule, sEntry);
		return result;
	}
	CString GetEntriesOfModule(LPCTSTR sModuleID, BOOL bActive, LPCTSTR sSep)
	{
		CString result;
		static BYTE parms[] = VTS_BSTR VTS_BOOL VTS_BSTR ;
		InvokeHelper(0xe3, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms, sModuleID, bActive, sSep);
		return result;
	}
	long AddModule(LPCTSTR sModuleID, LPCTSTR sModuleName, long iModuleType, long iLicences, BOOL bIsDemo, DATE dtExpires, long iNoOfDays, DATE dtMaxDate, LPCTSTR sTag, BOOL AllowDeactivate, long iWebActivation)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_I4 VTS_I4 VTS_BOOL VTS_DATE VTS_I4 VTS_DATE VTS_BSTR VTS_BOOL VTS_I4 ;
		InvokeHelper(0xe4, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModuleID, sModuleName, iModuleType, iLicences, bIsDemo, dtExpires, iNoOfDays, dtMaxDate, sTag, AllowDeactivate, iWebActivation);
		return result;
	}
	long ValidateEx(LPCTSTR sModule, LPCTSTR sUser, BOOL bUseWindowsUser, BSTR * sTag)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BOOL VTS_PBSTR ;
		InvokeHelper(0xe5, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModule, sUser, bUseWindowsUser, sTag);
		return result;
	}
	long SetLicViewerButton(long iButton, long iValue)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 ;
		InvokeHelper(0xe6, DISPATCH_METHOD, VT_I4, (void*)&result, parms, iButton, iValue);
		return result;
	}
	CString GetVal(LPCTSTR sModule, LPCTSTR sToken)
	{
		CString result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0xdb, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms, sModule, sToken);
		return result;
	}
	long SetVal(LPCTSTR sModule, LPCTSTR sToken, LPCTSTR sValue)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR ;
		InvokeHelper(0xdc, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModule, sToken, sValue);
		return result;
	}
	DATE GetLicFileDate()
	{
		DATE result;
		InvokeHelper(0xdd, DISPATCH_METHOD, VT_DATE, (void*)&result, NULL);
		return result;
	}
	long DeleteModule(LPCTSTR sModuleID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0xde, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModuleID);
		return result;
	}
	long GetLicFileID()
	{
		long result;
		InvokeHelper(0xe7, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long GetLicFileVersion()
	{
		long result;
		InvokeHelper(0xe8, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long ApplyActivationKey(LPCTSTR sKey)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0xe9, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sKey);
		return result;
	}
	long WebRegister(LPCTSTR sProjectname, LPCTSTR sModule, LPCTSTR sID, LPCTSTR sEMail)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR ;
		InvokeHelper(0xea, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sProjectname, sModule, sID, sEMail);
		return result;
	}
	long GetWebActivation(LPCTSTR sModule)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0xec, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModule);
		return result;
	}
	CString get_WebServiceURL()
	{
		CString result;
		InvokeHelper(0xed, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_WebServiceURL(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0xed, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	CString get_LocalProxyServer()
	{
		CString result;
		InvokeHelper(0xee, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_LocalProxyServer(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0xee, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_LocalProxyPort()
	{
		long result;
		InvokeHelper(0xef, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_LocalProxyPort(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xef, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_UseLocalIEProxy()
	{
		BOOL result;
		InvokeHelper(0xf0, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_UseLocalIEProxy(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0xf0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_UseLocalProxy()
	{
		BOOL result;
		InvokeHelper(0xf1, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_UseLocalProxy(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0xf1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long RemoveDeactivated(LPCTSTR sModuleID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0xf2, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModuleID);
		return result;
	}
	CString get_Projectname()
	{
		CString result;
		InvokeHelper(0xf3, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_Projectname(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0xf3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long ValidatesYes(LPCTSTR sModuleID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0xeb, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModuleID);
		return result;
	}
	BOOL get_ShowWAStartPage()
	{
		BOOL result;
		InvokeHelper(0xf4, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_ShowWAStartPage(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0xf4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_ShowWAResultPage()
	{
		BOOL result;
		InvokeHelper(0xf5, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_ShowWAResultPage(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0xf5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_ShowWAProgressPage()
	{
		BOOL result;
		InvokeHelper(0xf6, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_ShowWAProgressPage(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0xf6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long ApplyActivationKeyForProject(LPCTSTR sProjectname, LPCTSTR sKey)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0xf7, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sProjectname, sKey);
		return result;
	}
	long PrepareForce(LPCTSTR FileName, LPCTSTR PSecKey)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0xf8, DISPATCH_METHOD, VT_I4, (void*)&result, parms, FileName, PSecKey);
		return result;
	}
	void SetLogging(BOOL bOn, LPCTSTR sPath)
	{
		static BYTE parms[] = VTS_BOOL VTS_BSTR ;
		InvokeHelper(0xf9, DISPATCH_METHOD, VT_EMPTY, NULL, parms, bOn, sPath);
	}
	long _CheckLicence(LPCTSTR sProjectname, LPCTSTR sID, LPCTSTR sProduct, BOOL bForceCheck)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR VTS_BOOL ;
		InvokeHelper(0xfa, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sProjectname, sID, sProduct, bForceCheck);
		return result;
	}
	long DeactivateLicence(LPCTSTR sProjectname, LPCTSTR sID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0xfb, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sProjectname, sID);
		return result;
	}
	long get_LicenceVerification()
	{
		long result;
		InvokeHelper(0xfc, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_LicenceVerification(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xfc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	DATE get_NextVerification()
	{
		DATE result;
		InvokeHelper(0xfd, DISPATCH_PROPERTYGET, VT_DATE, (void*)&result, NULL);
		return result;
	}
	void put_NextVerification(DATE newValue)
	{
		static BYTE parms[] = VTS_DATE ;
		InvokeHelper(0xfd, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	CString GetModuleList(BOOL bWithNames, BOOL bWithTypes)
	{
		CString result;
		static BYTE parms[] = VTS_BOOL VTS_BOOL ;
		InvokeHelper(0xfe, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms, bWithNames, bWithTypes);
		return result;
	}
	long PrepareSimple(LPCTSTR FileName, LPCTSTR PSecKey)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0xff, DISPATCH_METHOD, VT_I4, (void*)&result, parms, FileName, PSecKey);
		return result;
	}
	long PrepareAdvanced(LPCTSTR FileName, LPCTSTR ReadKey)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0x100, DISPATCH_METHOD, VT_I4, (void*)&result, parms, FileName, ReadKey);
		return result;
	}
	CString GetCRC32(LPCTSTR sFilename)
	{
		CString result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x102, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms, sFilename);
		return result;
	}
	long ResetLocalRunNo(long iRunNo)
	{
		long result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x103, DISPATCH_METHOD, VT_I4, (void*)&result, parms, iRunNo);
		return result;
	}
	long SetVerificationID(LPCTSTR sID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x104, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sID);
		return result;
	}
	long get_SecurityLevel()
	{
		long result;
		InvokeHelper(0x105, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_SecurityLevel(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x105, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	CString GetVersion()
	{
		CString result;
		InvokeHelper(0x101, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	long SendCustomerinfo(LPCTSTR sProjectname, LPCTSTR sEntryKey, LPCTSTR sCompanyName, LPCTSTR sFirstName, LPCTSTR sLastName, LPCTSTR sSalutation, LPCTSTR sAcademicTitle, LPCTSTR sJobTitle, LPCTSTR sStreetAddress1, LPCTSTR sStreetAddress2, LPCTSTR sZIP, LPCTSTR sCity, LPCTSTR sState, LPCTSTR sCountry, LPCTSTR sPhone1, LPCTSTR sPhone2, LPCTSTR sEMail, LPCTSTR sLanguage, LPCTSTR sWebAddress, BOOL bNewsletter1, BOOL bNewsletter2, BOOL bNewsletter3, BOOL bNewsletter4, LPCTSTR sCustomer1, LPCTSTR sCustomer2, long iShowConnMask)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_BSTR VTS_BSTR VTS_I4 ;
		InvokeHelper(0x106, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sProjectname, sEntryKey, sCompanyName, sFirstName, sLastName, sSalutation, sAcademicTitle, sJobTitle, sStreetAddress1, sStreetAddress2, sZIP, sCity, sState, sCountry, sPhone1, sPhone2, sEMail, sLanguage, sWebAddress, bNewsletter1, bNewsletter2, bNewsletter3, bNewsletter4, sCustomer1, sCustomer2, iShowConnMask);
		return result;
	}
	DATE GetLastRunNoDate()
	{
		DATE result;
		InvokeHelper(0x107, DISPATCH_METHOD, VT_DATE, (void*)&result, NULL);
		return result;
	}
	long ResetGlobalRunNo(long iRunNo, long iResetDays)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 ;
		InvokeHelper(0x108, DISPATCH_METHOD, VT_I4, (void*)&result, parms, iRunNo, iResetDays);
		return result;
	}
	long RemoveLocalRunNo()
	{
		long result;
		InvokeHelper(0x109, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long SetVerificationProduct(LPCTSTR sProduct)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x10a, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sProduct);
		return result;
	}
	BOOL get_TamperDetection()
	{
		BOOL result;
		InvokeHelper(0x10b, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_TamperDetection(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x10b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_LicenceTampered()
	{
		BOOL result;
		InvokeHelper(0x10c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	long SetRegisterSettings(LPCTSTR sImageFile, BOOL bLicViewerAllowed, BOOL bSerialIsKey, LPCTSTR sManufacturer, LPCTSTR sProductname, BOOL bRegisterOnline, BOOL bRegisterByEMail, LPCTSTR sEMailAddress, LPCTSTR sEMailSubject, BOOL bRegisterByFax, LPCTSTR sFaxnumber, BOOL bRegisterByPhone, LPCTSTR sPhonenumber, LPCTSTR sEMailFaxText, long iDefaultMethod, LPCTSTR sBuyURL, LPCTSTR sHelpURL, LPCTSTR sCopyProtectionSwitchOnKey, LPCTSTR sCopyProtectionSwitchOffKey)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BOOL VTS_BOOL VTS_BSTR VTS_BSTR VTS_BOOL VTS_BOOL VTS_BSTR VTS_BSTR VTS_BOOL VTS_BSTR VTS_BOOL VTS_BSTR VTS_BSTR VTS_I4 VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR ;
		InvokeHelper(0x10d, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sImageFile, bLicViewerAllowed, bSerialIsKey, sManufacturer, sProductname, bRegisterOnline, bRegisterByEMail, sEMailAddress, sEMailSubject, bRegisterByFax, sFaxnumber, bRegisterByPhone, sPhonenumber, sEMailFaxText, iDefaultMethod, sBuyURL, sHelpURL, sCopyProtectionSwitchOnKey, sCopyProtectionSwitchOffKey);
		return result;
	}
	long SetRegisterHelp(LPCTSTR sHelptextWelcome, LPCTSTR sHelptextExpired, LPCTSTR sHelptextActivation1, LPCTSTR sHelptextActivation2, LPCTSTR sHelptextEnterKey, LPCTSTR sHelptextDone, LPCTSTR sHelptextErrorMsg)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR ;
		InvokeHelper(0x10e, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sHelptextWelcome, sHelptextExpired, sHelptextActivation1, sHelptextActivation2, sHelptextEnterKey, sHelptextDone, sHelptextErrorMsg);
		return result;
	}
	long PrepareAndCheck(LPCTSTR FileName, LPCTSTR PSK, LPCTSTR ModuleID, LPCTSTR Username, BOOL UseWindowsUser, BOOL DoQuit, BSTR * ModuleTag)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BOOL VTS_BOOL VTS_PBSTR ;
		InvokeHelper(0x10f, DISPATCH_METHOD, VT_I4, (void*)&result, parms, FileName, PSK, ModuleID, Username, UseWindowsUser, DoQuit, ModuleTag);
		return result;
	}
	long ApplyInvisibleKey(LPCTSTR sKey)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x110, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sKey);
		return result;
	}
	long ShowStartErrorPage(long iError)
	{
		long result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x111, DISPATCH_METHOD, VT_I4, (void*)&result, parms, iError);
		return result;
	}
	long SetRegisterLanguages(LPCTSTR sLanguages)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x112, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sLanguages);
		return result;
	}
	long Refresh()
	{
		long result;
		InvokeHelper(0x113, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_ConcurrentUserLevel()
	{
		long result;
		InvokeHelper(0x114, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_ConcurrentUserLevel(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x114, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long ValidateAndDecrease(LPCTSTR sModule)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x115, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModule);
		return result;
	}
	long ShowMessagePage(LPCTSTR sTitle, LPCTSTR sHeader, LPCTSTR sText, BOOL bShowLicViewer)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR VTS_BOOL ;
		InvokeHelper(0x116, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sTitle, sHeader, sText, bShowLicViewer);
		return result;
	}
	BOOL ExistsConfig(LPCTSTR sConfigname)
	{
		BOOL result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x117, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms, sConfigname);
		return result;
	}
	CString GetConfig(LPCTSTR sConfigname)
	{
		CString result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x118, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms, sConfigname);
		return result;
	}
	long SetConfig(LPCTSTR sConfigname, LPCTSTR sValue, BOOL bIsVisible)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BOOL ;
		InvokeHelper(0x119, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sConfigname, sValue, bIsVisible);
		return result;
	}
	BOOL IsConfigVisible(LPCTSTR sConfigname)
	{
		BOOL result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x11a, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms, sConfigname);
		return result;
	}
	long DeleteConfig(LPCTSTR sConfigname)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x11b, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sConfigname);
		return result;
	}
	long RemoveAllItems(LPCTSTR sModuleID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x11c, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModuleID);
		return result;
	}
	long SetRegisterHelpL(LPCTSTR sLanguage, LPCTSTR sHelptextWelcome, LPCTSTR sHelptextExpired, LPCTSTR sHelptextActivation1, LPCTSTR sHelptextActivation2, LPCTSTR sHelptextEnterKey, LPCTSTR sHelptextDone, LPCTSTR sHelptextErrorMsg)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR ;
		InvokeHelper(0x11d, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sLanguage, sHelptextWelcome, sHelptextExpired, sHelptextActivation1, sHelptextActivation2, sHelptextEnterKey, sHelptextDone, sHelptextErrorMsg);
		return result;
	}
	long Tick(LPCTSTR sModuleID, long iTicks)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_I4 ;
		InvokeHelper(0x11e, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModuleID, iTicks);
		return result;
	}
	long RunsOnVirtualMachine()
	{
		long result;
		InvokeHelper(0x11f, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long UploadLic(LPCTSTR sProjectname, LPCTSTR sModuleID, LPCTSTR sID, LPCTSTR sName, LPCTSTR sCode, BOOL bAllowCreateActivation)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BOOL ;
		InvokeHelper(0x120, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sProjectname, sModuleID, sID, sName, sCode, bAllowCreateActivation);
		return result;
	}
	long DownloadLic(LPCTSTR sProjectname, LPCTSTR sModuleID, LPCTSTR sID, LPCTSTR sName, LPCTSTR sCode, BOOL bDownloadFull)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BOOL ;
		InvokeHelper(0x121, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sProjectname, sModuleID, sID, sName, sCode, bDownloadFull);
		return result;
	}
	long GetVerificationOfModule(LPCTSTR sModuleID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x122, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModuleID);
		return result;
	}
	DATE GetNextVerificationOfModule(LPCTSTR sModuleID)
	{
		DATE result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x123, DISPATCH_METHOD, VT_DATE, (void*)&result, parms, sModuleID);
		return result;
	}
	DATE GetEndVerificationOfModule(LPCTSTR sModuleID)
	{
		DATE result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x124, DISPATCH_METHOD, VT_DATE, (void*)&result, parms, sModuleID);
		return result;
	}
	long CheckModule(LPCTSTR sProjectname, LPCTSTR sModuleID, LPCTSTR sID, BOOL bForceCheck, BOOL bRequestLicData)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR VTS_BOOL VTS_BOOL ;
		InvokeHelper(0x125, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sProjectname, sModuleID, sID, bForceCheck, bRequestLicData);
		return result;
	}
	long DeactivateModule(LPCTSTR sProjectname, LPCTSTR sModuleID, LPCTSTR sID, BOOL bSendLicData)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR VTS_BOOL ;
		InvokeHelper(0x126, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sProjectname, sModuleID, sID, bSendLicData);
		return result;
	}
	BOOL WouldModuleBeCheckedOnline(LPCTSTR sModuleID)
	{
		BOOL result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x127, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms, sModuleID);
		return result;
	}
	long WebRegisterExt(LPCTSTR sProjectname, LPCTSTR sModuleID, LPCTSTR sID, LPCTSTR sEMail, BOOL bRequestLicData)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BOOL ;
		InvokeHelper(0x128, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sProjectname, sModuleID, sID, sEMail, bRequestLicData);
		return result;
	}
	CString GetVerificationIDofModule(LPCTSTR sModuleID)
	{
		CString result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x129, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms, sModuleID);
		return result;
	}
	long SetVerificationOfModule(LPCTSTR sModuleID, long iVerification)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_I4 ;
		InvokeHelper(0x12a, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModuleID, iVerification);
		return result;
	}
	long SetNextVerificationOfModule(LPCTSTR sModuleID, DATE dt)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_DATE ;
		InvokeHelper(0x12b, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModuleID, dt);
		return result;
	}
	long SetEndVerificationOfModule(LPCTSTR sModuleID, DATE dt)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_DATE ;
		InvokeHelper(0x12c, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModuleID, dt);
		return result;
	}
	long SetVerificationIDofModule(LPCTSTR sModuleID, LPCTSTR sID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0x12d, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sModuleID, sID);
		return result;
	}
	long BeginReadCache()
	{
		long result;
		InvokeHelper(0x12e, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long BeginCache()
	{
		long result;
		InvokeHelper(0x12f, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long EndCache()
	{
		long result;
		InvokeHelper(0x130, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long RefreshCache()
	{
		long result;
		InvokeHelper(0x131, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long RevertCache()
	{
		long result;
		InvokeHelper(0x132, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long WriteCache()
	{
		long result;
		InvokeHelper(0x133, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long GetCacheMode()
	{
		long result;
		InvokeHelper(0x134, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	CString GetMailActivatorBlock(LPCTSTR sID, LPCTSTR sProjectname, LPCTSTR sName, LPCTSTR sEMail, LPCTSTR sModuleID, BOOL bIsForEMail)
	{
		CString result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BOOL ;
		InvokeHelper(0x135, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms, sID, sProjectname, sName, sEMail, sModuleID, bIsForEMail);
		return result;
	}
	BOOL IsKeyValid(LPCTSTR sKey)
	{
		BOOL result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x136, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms, sKey);
		return result;
	}
	CString GetKeyModule(LPCTSTR sKey)
	{
		CString result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x137, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms, sKey);
		return result;
	}
	CString GetKeyInstcode(LPCTSTR sKey)
	{
		CString result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x138, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms, sKey);
		return result;
	}
	long GetKeyType(LPCTSTR sKey)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x139, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sKey);
		return result;
	}
	long GetKeyLicences(LPCTSTR sKey)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x13a, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sKey);
		return result;
	}
	long GetKeyDays(LPCTSTR sKey)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x13b, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sKey);
		return result;
	}
	BOOL GetKeyAbsoluteLics(LPCTSTR sKey)
	{
		BOOL result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x13c, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms, sKey);
		return result;
	}
	BOOL GetKeyAbsoluteDays(LPCTSTR sKey)
	{
		BOOL result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x13d, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms, sKey);
		return result;
	}
	DATE GetKeyExpiryDate(LPCTSTR sKey)
	{
		DATE result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x13e, DISPATCH_METHOD, VT_DATE, (void*)&result, parms, sKey);
		return result;
	}
	long GetKeyDemo(LPCTSTR sKey)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x13f, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sKey);
		return result;
	}
	long GetKeyActivation(LPCTSTR sKey)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x140, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sKey);
		return result;
	}
	BOOL GetKeyForceOnlineCheck(LPCTSTR sKey)
	{
		BOOL result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x141, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms, sKey);
		return result;
	}
	BOOL GetKeyLocalReuse(LPCTSTR sKey)
	{
		BOOL result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x142, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms, sKey);
		return result;
	}
	long GetKeyCopyProtectionType(LPCTSTR sKey)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x143, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sKey);
		return result;
	}
	CString GetLPComputername()
	{
		CString result;
		InvokeHelper(0x144, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	CString GetLPWindowsUser()
	{
		CString result;
		InvokeHelper(0x145, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	long GetLanguage()
	{
		long result;
		InvokeHelper(0x146, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long GetKeyFileID(LPCTSTR sKey)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x147, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sKey);
		return result;
	}
	BOOL ExistsModule(LPCTSTR sModuleID)
	{
		BOOL result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x148, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms, sModuleID);
		return result;
	}
	long SetRegisterSerialNo(LPCTSTR sSerialNo)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x149, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sSerialNo);
		return result;
	}
	BOOL IsKeyForThisSystem(LPCTSTR sKey)
	{
		BOOL result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x14a, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms, sKey);
		return result;
	}
	long GetKeyYesNo(LPCTSTR sKey)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x14b, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sKey);
		return result;
	}
	long GetKeyResetGraceDays(LPCTSTR sKey)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x14c, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sKey);
		return result;
	}

	// ILicProtectorEasyGo270 properties
public:

};
