
// NorrLicense.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "NorrLicense.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CNorrLicenseApp

BEGIN_MESSAGE_MAP(CNorrLicenseApp, CWinAppEx)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CNorrLicenseApp construction

CNorrLicenseApp::CNorrLicenseApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CNorrLicenseApp object

CNorrLicenseApp theApp;


// CNorrLicenseApp initialization

CString CNorrLicenseApp::GetAppDir()
{
	CString dir(theApp.m_pszHelpFilePath);
	dir = dir.Left(dir.ReverseFind('\\') + 1);
	return dir;
}


BOOL CNorrLicenseApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinAppEx::InitInstance();

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
//	SetRegistryKey(_T("NorrLicense"));

	if (!AfxOleInit())
	{
		return FALSE;
	}

	// Create licence automation object
	if( !m_LicProtector.CreateDispatch(_T("LicProtectorEasyGo.LicProtectorEasyGo270")) )
	{
		AfxMessageBox(_T("Automation object not found (LicProtector270.dll)."));
		return false;
	}

	int ret;
	m_LicProtector.put_SecurityLevel(0);
	if( (ret = m_LicProtector.Prepare(GetAppDir() + _T("NorrServer.lic"), _T("2853657132"))) )
	{
		AfxMessageBox(m_LicProtector.GetErrorMessage(ret));
		m_LicProtector.ReleaseDispatch();
		return false;
	}

	m_LicProtector.ShowLicViewer();

	return FALSE;
}
