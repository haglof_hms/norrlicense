
// NorrLicense.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols
#include "LicProtectorEasyGo270.h"

// CNorrLicenseApp:
// See NorrLicense.cpp for the implementation of this class
//

class CNorrLicenseApp : public CWinAppEx
{
public:
	CNorrLicenseApp();
	
	CLicProtectorEasyGo270 m_LicProtector;

// Overrides
	public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
	CString GetAppDir();
};

extern CNorrLicenseApp theApp;
